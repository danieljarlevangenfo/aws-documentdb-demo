
# DocumentDB demo

Debug profile included for Visual Studio Code.

## Prerequisties
* node >= 8.10

## Project Setup
* run ```npm i``` in root of project
* Deploy a DocumentDB cluster. See: [Getting Started with Amazon DocumentDB](https://docs.aws.amazon.com/documentdb/latest/developerguide/getting-started.html) TODO: use Serverless.
* Deploy an EC2 instance in the same VPC. TODO: use Serverless.
* Use Robo3T/Studio3T/MongoCompass to access db using SSH tunnel.
* Make sure Lambdas are deployed in the same VPC/Security group as DocumentDB.
* Postman templates included in project root.

## Running and debugging lambdas locally
```npm start``` Will start a server on localhost:3000 that you can use like you would live.
there is no need to run this command again as it will automatically update on changes
```in vscode press f5 and chose Debug to run local lambdas with debugger``` Will also start the localhost:3000. Just like npm start

## Local mongo instance
Set localhost:port for the local environment in `env.yml`.

## Invoking functions
```serverless invoke local -f <functionName> -p <path to event.json>```

## Deploying serverless functions
Make sure AWS crendetials are set in environment variables and run `npm run deploy`.

TODO: When deploying via git the serverless functions will be deployed automatically via the ```bitbucket-pipelines.yml``` file.

### More info:
* https://nodejs.org/en/
* https://serverless.com/
* https://www.typescriptlang.org/
* https://webpack.js.org/
* https://palantir.github.io/tslint/
* https://www.typescriptlang.org/docs/handbook/integrating-with-build-tools.html#webpack

## Known bugs
[Serverless fails if provided profile is not valid](https://github.com/serverless/serverless/pull/4245)