var pipeline = [
    { $sort: { lastName: 1 } },
    { $group: { _id: '$lastName', firstNames: { $push: '$firstName' } } }
]
db.getCollection('users').aggregate(pipeline);