var pipeline = [
    { $sort: { lastName: 1 } },
    { $group: { _id: '$lastName', count: { $sum: 1 } } },
    { $sort: { count: -1 } }
]
db.getCollection('users').aggregate(pipeline);