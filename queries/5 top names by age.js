var pipeline = [
    { $sort: { lastName: 1 } },
    { $group: { _id: '$lastName', count: { $sum: 1 }, user: { $push: { firstName: '$firstName', age: '$age' } } } },
    { $sort: { count: -1 } },
    { $limit: 1 },
    { $unwind: { path: '$user' } },
    { $sort: { 'user.age': -1 } },
    { $limit: 5 }
]
db.getCollection('users').aggregate(pipeline);