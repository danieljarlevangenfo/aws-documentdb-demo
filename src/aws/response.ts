const ok = (responseData: string | object, statusCode = 200) => {
  return {
    statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': typeof responseData === 'string' ? 'text/plain' : 'application/json'
    },
    body: typeof responseData === 'string' ? responseData : JSON.stringify(responseData)
  };
};

const clientError = (responseData: string | object, statusCode = 400) => {
  return {
    statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': typeof responseData === 'string' ? 'text/plain' : 'application/json'
    },
    body: typeof responseData === 'string' ? responseData : JSON.stringify(responseData)
  };
};

const serverError = (responseData: string | object, statusCode = 500) => {
  return {
    statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': typeof responseData === 'string' ? 'text/plain' : 'application/json'
    },
    body: typeof responseData === 'string' ? responseData : JSON.stringify(responseData)
  };
};

export { ok, clientError, serverError };
