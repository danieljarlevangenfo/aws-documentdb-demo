import { db } from '../io/mongo';
import { ok } from '../aws/response';
import faker = require('faker');
import crypto = require('crypto');

const dbConnection = db();

export const query = async (event: AWSLambda.APIGatewayEvent, context: AWSLambda.Context) => {
    const db = await dbConnection;
    const body = event.body && JSON.parse(event.body) || { collection: 'users', query: {}, aggregation: null, projection: { _id: 0 } };
    const { collection, query, aggregation, projection } = body;
    let result;
    if (query && collection) {
        result = await db.collection(collection).find(query).project(projection).toArray();
    } else if (aggregation && collection) {
        result = await db.collection(collection).aggregate(aggregation).toArray();
    } else if (collection) {
        result = { collection, count: await db.collection(collection).count({}) };
    } else {
        result = { collections: await db.collections() };
    }
    return ok(result);
}

export const seed = async (event: AWSLambda.APIGatewayEvent, context: AWSLambda.Context) => {
    const db = await dbConnection;

    const target = event.pathParameters && parseInt(event.pathParameters.target, 10) || 1000;

    let start = Date.now();

    // change locale for the random data if you want
    //faker.locale = 'sv';
    console.log(`Generating ${target} fake users...`);

    const users = Array.from({ length: target }).map((_, id) => {
        const firstName = faker.name.firstName();
        const lastName = faker.name.lastName();
        const user = {
            id: crypto.randomBytes(4).readUIntBE(0, 4),
            firstName,
            lastName,
            email: `${firstName}.${lastName}@${faker.internet.domainName()}`.toLowerCase(),
            age: 18 + faker.random.number(70),
            length: 155 + faker.random.number(35),
            profileImage: faker.internet.avatar(),
            likesIceCream: faker.random.boolean,
            address: {
                street: faker.address.streetAddress(),
                zip: faker.address.zipCode(),
                city: faker.address.city(),
                country: faker.address.country()
            },
            created: faker.date.past(),
        }
        return {
            insertOne: {
                document: user,
            },
        };
    });
    let stop = Date.now();
    console.log('Done (%s ms). Writing to DB...', (stop - start));

    await db.collection('users').bulkWrite(users);
    console.log('Done (%s ms).', (Date.now() - stop));
    return ok({ message: 'OK', inserted: target });
}
