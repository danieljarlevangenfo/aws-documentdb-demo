
import { Db, MongoClient } from 'mongodb';
import caBundle from './rds-combined-ca-bundle';
import sshKey from './ssh-key';
import tunnel = require('tunnel-ssh');

let client: MongoClient;
let dbInstance: Db;
let connecting: Promise<Db>;

const defaultConnection = {
    url: process.env.MONGO_URL || 'mongodb://localhost:27017',
    db: process.env.MONGO_DB || 'demo',
};
const defaultSettings: MongoConnectionSettings = {
    ssl: process.env.MONGO_SSL === 'true',
    sslValidate: process.env.MONGO_SSL_VALIDATE_CERT === 'true',
    sslCA: caBundle,
    useNewUrlParser: true,
    reconnectTries: 5,
    reconnectInterval: 1000,
    poolSize: 5,
    authSource: 'admin',
    appname: 'documentdb-demo',
};

export function db(connection: MongoSettings = defaultConnection, settings = defaultSettings) {
    if (dbInstance && client && client.isConnected()) {
        console.log('Mongo: Connection already established.');
        return Promise.resolve(client.db(connection.db));
    } else {
        if (connecting) {
            console.log('Mongo: Returning pending connection...');
            return connecting;
        }
        console.log('Mongo: Returning new pending connection...');
        connecting = new Promise<Db>(async (resolve, reject) => {
            try {
                console.log('Mongo: Attempting to connect to Mongo at %s (SSL: %s, validate cert: %s)...', connection.url, settings.ssl, settings.sslValidate);
                MongoClient.connect(connection.url, settings, (error: any, mongoClient: MongoClient) => {
                    if (error) {
                        console.error('Mongo: Connection to Mongo failed.', error);
                        return reject(error);
                    }
                    client = mongoClient;
                    console.log('Mongo: Connection to Mongo established.');
                    dbInstance = client.db(connection.db);
                    return resolve(dbInstance);
                });
            } catch (error) {
                console.error('Mongo: Error while connecting to Mongo', error);
                reject('Mongo: Error while connecting to Mongo.');
            }
        });
        return connecting;
    }
}


export async function sshTunnelExample(settings: MongoSettings) {

    const sshCfg = {
        host: 'ec2-111-111-111-111.eu-west-1.compute.amazonaws.com', // ssh host
        username: 'ubuntu',  // EC2 user
        privateKey: sshKey,  // imported private key for ssh connection
        port: 22,
        dstHost: 'acaconfig-dev-2019-02-20-14-20-24.cluster-cadx24svnl5y.eu-west-1.docdb.amazonaws.com', // target url to forward requests to
        dstPort: 27017,
        localHost: 'localhost', // where to send requests locally and have them and up on dstHost instead
        localPort: 27027
    };

    settings.url = 'mongodb://root:<password>@localhost:27027/';

    return new Promise((resolve, reject) => {

        console.log('SSH: Setting up connection to host at %s:%s...', sshCfg.host, sshCfg.port);
        tunnel(sshCfg, async (error: any, server: any) => {
            if (error) {
                console.log('SSH: Connection error: ', error);
                return reject(error);
            } else {
                console.log('SSH: Tunnel established (traffic to %s:%s will be tunneled to %s:%s).', sshCfg.localHost, sshCfg.localPort, sshCfg.dstHost, sshCfg.dstPort);
                console.log('MongoIO: Connecting to %s...', settings.url);
                try {
                    client = await MongoClient.connect(settings.url, {
                        useNewUrlParser: true,
                        ssl: true,
                        sslValidate: false,
                        sslCA: caBundle
                    } as MongoConnectionSettings);
                    dbInstance = client.db(settings.db);
                    console.log('MongoIO: Connected to db.');
                    return resolve(dbInstance);
                } catch (error) {
                    console.error('MongoIO: Unable to connect to db.', error);
                    return reject(error);
                }
            }
        });
    });

}

export interface MongoSettings {
    url: string;
    db: string;
}

export interface MongoConnectionSettings {
    [key: string]: string | number | boolean;
}
