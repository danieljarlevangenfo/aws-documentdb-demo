const path = require('path');
const slsw = require('serverless-webpack');

module.exports = {
  // devtool: 'eval', // faster if you don't care about debugging
  devtool: 'source-map',
  entry: slsw.lib.entries,
  mode: 'development',
  externals: {
    'aws-sdk': 'aws-sdk',
    'utf-8-validate': 'utf-8-validate',
    'bufferutil': 'bufferutil',
  },
  resolve: {
    symlinks: false,
    extensions: ['.js', '.json', '.ts'],
  },
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
  },
  target: 'node',
  watchOptions: {
    ignored: [/node_modules/, /dist/, /\.serverless/, /\.vscode/],
  },
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        include: path.resolve(__dirname, 'src'),
        loader: 'ts-loader',
        options: {},
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: "javascript/auto",
      },
    ],
  },
};
